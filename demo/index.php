<?php 
    $currentSite = 'home';
?>
<!doctype html>
<html xmlns:fb="http://ogp.me/ns/fb#">
    
<head>
    <meta charset="utf-8">
    <title>Ajax page swap with history support and optional effects with jQuery Kool Swap!</title>
    <meta name="author" content="Joscha Schmidt">
    <meta name="description" content="Ajax page swap with history support and optional effects with jQuery Kool Swap!">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/script_head.php'; 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/link_head.php'; 
	?>
</head>

<body>
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/page_header.php'; 
    ?>
    <div class="row">
    	<div class="col-md-5 col-md-push-7 text-center">
    		<div class="arrow-top-box">
    			Switch between miscellaneous swap directions and speeds and navigate the website to see the plugin
    			in action. 
    		</div>
    		
    		<div class="text-center">
        		<a class="dl" href="https://github.com/griffla/Kool-Swap"><span aria-hidden="true" data-icon="&#xe006;"></span> Download on GitHub</a>
    		</div>
    		
    	</div>
    	
    	<div class="col-md-7 col-md-pull-5">
	        <h1>Swap pages in a cool and functional way</h1>
            <?php 
            	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/social_box.php'; 
            ?>
        </div>
    </div>
    
    <div class="row">
    	<div class="col-md-8">
	        <p class="lead kool-swap">
	            Kool Swap enables impressively ajax page load support for websites. Just <a href="setup.php">tell the plugin</a> what should be switched and swap pages in a nice way!<br>
	        </p>
    	</div>
    </div>
    
    <h2>Advantages</h2>
    <section class="infoBox clear kool-swap row" id="advantages" style="margin-bottom:20px">
        <div class="col-md-4">
        	<div class="info">
                <h2>Performance</h2>
                Scripts, stylesheets and other sitewide similar elements are loaded only once. Just a small amount of data is requested via ajax. 
        	</div>
        </div> 
        
        <div class="col-md-4">
        	<div class="info">
                <h2>History</h2>
                The url changes on page switch because of the intregated html5 history API. Browsers forward and back buttons work!
        	</div>
        </div> 
        
        <div class="col-md-4">
        	<div class="info">
                <h2>Callback events</h2>
                document.ready() functions can be executed with plugin triggered <a href="/demo/documentation.php#events">callback events</a> after load or animation of the loaded contents.
        	</div>
        </div>
	</section>
	
    <section class="infoBox clear kool-swap row" style="margin-bottom:20px">
        <div class="col-md-4">
        	<div class="info">
                <h2>Title, classes and IDs are transfered</h2>
                The plugin extracts the <code>&lt;title&gt;</code> as well as <code>&lt;html&gt;</code> and <code>&lt;body&gt;</code> classes and ids and replaces them with the swapBoxIn page values. 
        	</div>
        </div>

        <div class="col-md-4">
        	<div class="info">
                <h2>Types and multiple instances</h2>
                Use Kool Swap for sitewide functionality including history and title, class, id transfer and/or for <a href="/demo/examples/multiple-instances.php">content boxes on a page</a>.
        	</div>
        </div>
        
        <div class="col-md-4">
        	<div class="info">
                <span aria-hidden="true" data-icon="&#xe005;"></span><span>URL hashes are captured and browser functionality is emulated with scrollTo<span><br>
                <span aria-hidden="true" data-icon="&#xe005;"></span><span>Images are preloaded by default. <a href="/demo/examples/lots-of-images.php">Demo</a><span><br>
                <span aria-hidden="true" data-icon="&#xe005;"></span><span>The sitewide initialisation will only happen if the browser supports html5 history API. Links are requested with standard browser functionality and without ajax as fallback.<span><br>
        	</div>
        </div>
    </section>
    
	<h2>Good to know</h2>
    <section class="infoBox clear kool-swap row">
        <div class="col-md-4">
            <div class="info kool-swap">
                <h2>Remember it's beta!</h2><br>
                Kool Swap should someday simply work on every website also on existing sites but it was only tested by myself yet. <a href="/demo/examples/index.php#itsBeta">Read more.</a>  
            </div>
        </div>
            
        <div class="col-md-4">
            <div class="info kool-swap">
                <h2>Sitewide support for IE &lt; 10</h2><br>
                Internet Explorer 10 is the first version of IE supporting pushstate. IE 9 and below will fallback to standard page call.
            </div>
        </div>
    </section>
		        
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/page_footer.php'; 
    ?>
</body>
</html>
