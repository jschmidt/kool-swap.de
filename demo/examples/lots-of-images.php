<?php 
    $currentSite = '';
?>
<!doctype html>
<html xmlns:fb="http://ogp.me/ns/fb#">
    
<head>
    <meta charset="utf-8">
    <title>Lots of images - Images are preloaded by Kool Swap!</title>
    <meta name="author" content="Joscha Schmidt">
    <meta name="description" content="Images can optionally be preloaded by Kool Swap">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/script_head.php'; 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/link_head.php'; 
	?>
</head>

<body>
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/page_header.php'; 
    ?>
    <h1>Lots of images</h1>
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/social_box.php'; 
    ?>
    
    <div class="row">
        <div class="col-md-12 img-prev">
        
            <p>Images are preloaded before the page swap by default. Set the option "preloadImages" to false if you don't need this.</p> 
            
            <p class="info">
                <span aria-hidden="true" data-icon="&#xe004;"></span>
                Photos by <a href="http://www.photocase.com">Photocase</a>.
            </p> 
            
            <img src="/demo/images/photocase149115992.jpg">
            <img src="/demo/images/photocase449256731238.jpg">
            <img src="/demo/images/photocase454k9nzuxfed.jpg">
            <img src="/demo/images/photocase47nedyaq4n7v.jpg">
            <img src="/demo/images/photocase494816886977.jpg">
            <img src="/demo/images/photocase4abyzhkd42e8.jpg">
            <img src="/demo/images/photocase4p6hskq6pmpb.jpg">
            <img src="/demo/images/photocase569314655571.jpg">
            <img src="/demo/images/photocase781147486196.jpg">
            <img src="/demo/images/photocase88iajo8j51058612.jpg">

        </div>
    </div>
		        
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/page_footer.php'; 
    ?>
</body>
</html>