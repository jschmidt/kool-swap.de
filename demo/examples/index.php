<?php 
    $currentSite = '';
?>
<!doctype html>
<html xmlns:fb="http://ogp.me/ns/fb#">
    
<head>
    <meta charset="utf-8">
    <title>How to & examples - Kool Swap!</title>
    <meta name="author" content="Joscha Schmidt">
    <meta name="description" content="Ajax page swap with history support and optional effects with jQuery Kool Swap!">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/script_head.php'; 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/link_head.php'; 
	?>
</head>

<body>
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/page_header.php'; 
    ?>
    <h1>Good to know</h1>
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/social_box.php'; 
    ?>
    <div class="row">
        <div class="col-md-6">
            <h2>How it works</h2>
            
            <p>
                After a Kool Swap trigger click the plugin creates a temporary container (swapBoxIn) and loads the target page contents inside. It gets the 
                coordinates of the defined swapBox to position it absolute before animating. After this the swapBox animates out and the swapBoxIn animates in.
                Title, HTML/BODY classes and ids of the called page will be transfered. At best there will be no HTML/CSS optimisation required. 
            </p>
        </div>
        <div class="col-md-6">
            <h2 id="itsBeta">Remeber it's beta!</h2>
    
            <p>
                Kool Swap should someday simply work on every website also on existing sites but it was only tested by myself yet. 
                I tested it on several Websites and it worked fine but there may (better: will!) be cases were it won't work.<br>
                So please <a href="https://github.com/griffla/Kool-Swap/issues">report issues</a>.
            </p> 
        </div>
    </div>
    
    <h1>Tips &amp; Examples</h1>
    <div class="row">
        <div class="col-md-12">
            <table>
                <thead>
                    <tr>
                        <th>Topic</th>
                        <th>Description</th>
                        <th></th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr>
                        <td>Bouncing Boxes</td>
                        <td>Bouncing Boxes can be faded out/in on page swap. Consider the demo boxes at the bottom of the page and how they jump if they are not faded.</td>
                        <td><a href="/demo/examples/bouncing-boxes.php">See an example</a></td>
                    </tr>
                    
                    <tr>
                        <td>Section Use</td>
                        <td>Use Kool Swap for sitewide functionality including history and title/class/id transfer and/or for content boxes on a page.</td>
                        <td class="kool-swap"><a href="/demo/examples/multiple-instances.php">See an example</a></td>
                    </tr>
                    
                    <tr>
                        <td>Images</td>
                        <td>Images are preloaded before the page swap by default. Set the option "preloadImages" to false if you don't need this.</td>
                        <td class="kool-swap"><a href="/demo/examples/lots-of-images.php">See an example</a></td>
                    </tr>
                </tbody>
            </table>
        </div>
    </div>
		        
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/page_footer.php'; 
    ?>
</body>
</html>