<?php 
    $currentSite = '';
?>
<!doctype html>
<html xmlns:fb="http://ogp.me/ns/fb#">
    
<head>
    <meta charset="utf-8">
    <title>Bouncing Boxes - jQuery Kool Swap!</title>
    <meta name="author" content="Joscha Schmidt">
    <meta name="description" content="Bouncing Boxes like jumpy/jumping siblings can be avoid with the bouncingBoxes option">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/script_head.php'; 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/link_head.php'; 
	?>
</head>

<body>
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/page_header.php'; 
    ?>
    <h1>Bouncing Boxes</h1>
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/social_box.php'; 
    ?>
    
    <div class="kool-swap">
        <p>
            Bouncing boxes can be faded out/in or slided up/down on page swap (<a href="/demo/documentation.php#bouncingBoxHandling">bouncingBoxHandling</a>). This will mostly be non-positioned boxes below the defined swapBox. Consider the demo siblings at the bottom of the page and how they bounce if they are not faded.
        </p>
        
        <p>
            <a href="bouncing-boxes.php" id="justReload">Reload page without fading the bouncing boxes</a>
        </p>
        
        <p>
            <a href="bouncing-boxes.php" id="fadeSiblingAndReload">Reload page with bouncingBoxes-option defined</a>
        </p>
    </div>
    
    <pre>
        <code>$.koolSwap({<br>&nbsp;&nbsp;&nbsp;bouncingBoxes : '#bouncingBox1, #bouncingBox2',<br>});</code>
    </pre>
		        
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/page_footer.php'; 
    ?>
        
    <section class="warning" id="bouncingBox1"><code>#bouncingBox1</code> This section is defined under the swapBox and will jump to the top after click on a menu item.</section>
    <section class="warning" id="bouncingBox2"><code>#bouncingBox2</code> Another demo bounce box.</section>
    
    <script src="/demo/js/jquery.animate-enhanced.min.js"></script>
    
</body>
</html>