<?php 
    $currentSite = '';
?>
<!doctype html>
<html xmlns:fb="http://ogp.me/ns/fb#">
    
<head>
    <meta charset="utf-8">
    <title>Section use - Use Kool Swap for whole pages or content sections</title>
    <meta name="author" content="Joscha Schmidt">
    <meta name="description" content="Kool Swap can also be used for content sections additionally or instead of using it for whole sites">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/script_head.php'; 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/link_head.php'; 
	?>
</head>

<body>
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/page_header.php'; 
    ?>
	<h1>Section Use</h1>
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/social_box.php'; 
    ?>
	
	<div class="row">
		<div class="col-md-12">
            
            <p>Kool Swap can be used sitewide and/or with a selector.</p> 
            
            <ul class="tabs">
                <li><a href="/demo/index.php" data-ks-load-selector="#advantages">Kool Swap Advantages</a></li>
                <li><a href="/demo/documentation.php" data-ks-load-selector="#events">Events</a></li>
                <li><a href="/demo/documentation.php" data-ks-load-selector="#methods">Methods</a></li>
            </ul>
            
            <section id="tabContent" class="tabContent">
                Click a tab to switch content!
            </section>
            
            <div class="description">
	            <h2>Usage</h2>
	
				<p>Unlike the sitewide initialisation the selector has to be added in to the initialisation code:</p>
				
	            <div>
	                <pre>
	                    <code class="kool-swap">$('#tabContent').koolSwap({<br>&nbsp;&nbsp;&nbsp;swapTriggerBox : '.tabs',<br>&nbsp;&nbsp;&nbsp;<a href="/demo/documentation.php#bouncingBoxes">bouncingBoxes</a> : '.description, footer',<br>&nbsp;&nbsp;&nbsp;<a href="/demo/documentation.php#bouncingBoxHandling">bouncingBoxHandling</a> : 'slide',<br>&nbsp;&nbsp;&nbsp;direction: 'left-to-right',<br>&nbsp;&nbsp;&nbsp;<a href="/demo/documentation.php#moveSwapBoxClasses">moveSwapBoxClasses</a> : true,<br>&nbsp;&nbsp;&nbsp;<a href="/demo/documentation.php#positionType">positionType</a> : 'absolute',<br>&nbsp;&nbsp;&nbsp;outEasing : '.easeInOutCirc',<br>&nbsp;&nbsp;&nbsp;inEasing : 'easeOutBack',<br>});</code>
	                </pre>
	            </div>
	
				<p>Define the contents with the href to the target page and a selector with the <code>data-ks-load-selector</code> attribute to load the contents from:</p>
				
	            <div>
	                <pre>
	                    <code>&lt;a href="/demo/options.html" data-ks-load-selector="#events"&gt;Events&lt;/a&gt;</code>
	                </pre>
	            </div>
	            
                <p class="info kool-swap">
                	<span aria-hidden="true" data-icon="&#xe004;"></span>
	                Remember to reinitiate with the <a href="/demo/documentation.php#events">ksSwapCallback</a> event.
	            </p> 
	       </div>
		</div>
	</div>
		        
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/page_footer.php'; 
    ?>
</body>
</html>