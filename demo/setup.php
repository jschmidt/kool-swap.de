<?php 
    $currentSite = 'setup';
?>
<!doctype html>
<html id="thehow">
    
<head>
    <meta charset="utf-8">
    <title>Setup Kool Swap</title>
    <meta name="author" content="Joscha Schmidt">
    <meta name="description" content="A demonstration how to setup Kool Swap on your Website">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/script_head.php'; 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/link_head.php'; 
	?>
</head>

<body>
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/page_header.php'; 
    ?>
	<div class="row">
		<div class="col-md-12">
	        <h1>Setup Kool Swap</h1>
            <?php 
            	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/social_box.php'; 
            ?>
	        
	        <h2>Include jQuery</h2>
	        <pre>
				<code>&lt;script src="//ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"&gt;&lt;/script&gt;<br>&lt;script src="https://ajax.googleapis.com/ajax/libs/jqueryui/1.8.23/jquery-ui.min.js"&gt;&lt;/script&gt;</code>
	        </pre>
	
	        <p class="info">
	            <span aria-hidden="true" data-icon="&#xe004;"></span>
	            Include jQuery UI if you're using easings for animation. Kool Swap actually only needs the jQuery UI effects core.
	        </p> 
	        
	        <h2>Include the Kool Swap CSS and JavaScript</h2>
	        <pre>
	            <code>&lt;link rel="stylesheet" type="text/css" href="jquery.kool-swap/css/kool-swap.css"&gt;</code><br><code>&lt;script src="jquery.kool-swap/js/jquery.kool-swap.js"&gt;&lt;/script&gt;</code>
	        </pre>
	        
	        <h1>Activation</h1>
	        
	        <h2>Sitewide use</h1>
	            
	        <p>With url change and history as well as title, id and class transfer.</p>
	
	        <div id="plugin-activation">
	            <pre>
	                <code>$.koolSwap({<br>&nbsp;&nbsp;&nbsp;swapBox : '#main',<br>});</code>
	            </pre>
	        </div>
	
	        <p class="warning">
	            <span aria-hidden="true" data-icon="&#xe003;"></span>
	            Kool Swap uses the ID from swapBox to load the contents from the next page in sitewide mode. Please be aware that the defined swapBox has an ID. An element with the same ID must exist on the target
	            page.
	        </p> 
	        
	        <h2 id="section-use">Section use</h2>
	
	        <div class="kool-swap" id="plugin-activation">
	            <pre>
	                <code>$('<em>Element within content swaps</em>').koolSwap({<br>&nbsp;&nbsp;&nbsp;swapTriggerBox : '<em>Element that contains the triggers</em>',<br>&nbsp;&nbsp;&nbsp;<a href="/demo/documentation.php#moveSwapBoxClasses">moveSwapBoxClasses</a> : true,<br>&nbsp;&nbsp;&nbsp;<a href="/demo/documentation.php#positionType">positionType</a> : 'absolute',<br>});</code>
	            </pre>
	            <a href="/demo/examples/multiple-instances.php">Read more about the section use</a>
	        </div>
	        
	        <h1>Define triggers</h1>
	        
	        <p>You need to tell Kool Swap which links should trigger a kool swap.</p>
	        
	        <h2>Define triggers with option</h2>
	
	        <div id="triggers-with-option">
	            <pre>
	                <code>$.koolSwap({<br>&nbsp;&nbsp;&nbsp;swapBox : '#main',<br>&nbsp;&nbsp;&nbsp;swapTriggerBox: 'nav ul.main-menu'<br>});</code>
	            </pre>
	        </div>
	        
	        
	        <h2>Define triggers with CSS class</h2>
	        
	        <p>The default <a href="documentation.php#swap-trigger">swapTrigger</a> is "a". All swapTriggers in a box with given class "kool-swap" will trigger a kool swap.</p>
	
	        <div id="triggers-with-class">
	            <pre>
	                <code>&lt;ul class="main-menu <b>kool-swap</b>"&gt;<br>&nbsp;&nbsp;&nbsp;&lt;li&gt;&lt;a href="home.html"&gt;Home&lt;/a&gt;&lt;/li&gt;<br>&nbsp;&nbsp;&nbsp;&lt;li&gt;&lt;a href="about.html"&gt;About&lt;/a&gt;&lt;/li&gt;<br>&nbsp;&nbsp;&nbsp;&lt;li&gt;&lt;a href="contact.html"&gt;Contact&lt;/a&gt;&lt;/li&gt;<br>&lt;/ul&gt;</code>
	            </pre>
	        </div>
	        
	        <h2>That's it!</h2>
	        
	        <p class="kool-swap">Use the <a href="documentation.php">options</a> to configure Kool Swap.</p>
		
		</div>
	</div>
		        
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/page_footer.php'; 
    ?>
</body>
</html>