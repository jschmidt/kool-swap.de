<div class="social-box">
	<div class="s-plugin fb">
        <fb:like href="http://kool-swap.itsjoe.de" width="The pixel width of the plugin" height="The pixel height of the plugin" colorscheme="light" layout="button_count" action="like" show_faces="false" send="false"></fb:like>
	</div>
	
	<div class="s-plugin google">
        <div class="g-plusone" data-size="medium"></div>
	</div>

	<div class="s-plugin twitter">
		<a href="https://twitter.com/share" class="twitter-share-button" data-url="http://kool-swap.itsjoe.de" data-via="joscha_schmidt"></a>
	</div>
</div>

