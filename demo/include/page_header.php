<div id="fb-root"></div>
<script>(function(d, s, id) {
  var js, fjs = d.getElementsByTagName(s)[0];
  if (d.getElementById(id)) return;
  js = d.createElement(s); js.id = id;
  //js.src = "//connect.facebook.net/de_DE/all.js#xfbml=1&appId=490439857720243";
  js.src = "//connect.facebook.net/de_DE/all.js#xfbml=1";
  fjs.parentNode.insertBefore(js, fjs);
}(document, 'script', 'facebook-jssdk'));</script>

<div class="container">
    <header>
    	<div class="row">
    		<div class="col-md-7 col-sm-12">
    			<nav class="kool-swap clearfix">
    				<ul id="main-menu">
    					<li><a class="logo" href="/demo/" data-menu="home">Kool Swap</a></li>
    					<li><a href="/demo/setup.php" data-menu="setup">Setup</a></li>
    					<li><a href="/demo/documentation.php" data-menu="documentation">Documentation</a></li>
    					<li><a href="/demo/examples" data-menu="how-to">How to use</a></li>
    				</ul>
    			</nav>
    		</div>
    		<div class="col-md-5 col-sm-12 text-center" id="settings">
    			
    	        <div class="button-box type clearfix">
    	            <button data-anim-type="ksFade" class="swap-btn type" title="Fade"><span aria-hidden="true" data-icon="&#xe02d;"></span></button>
    	            <button data-anim-type="ksBottomToTop" class="swap-btn type" title="Animate bottom to top"><span aria-hidden="true" data-icon="&#xe025;"></span></button>
    	            <button data-anim-type="ksLeftToRight" class="swap-btn type" title="Animate left to right"><span aria-hidden="true" data-icon="&#xe027;"></span></button>
    	            <button data-anim-type="ksTopToBottom" class="active swap-btn type" title="Animate top to bottom"><span aria-hidden="true" data-icon="&#xe02b;"></span></button>
    	            <button data-anim-type="ksRightToLeft" class="swap-btn type" title="Animate right to left"><span aria-hidden="true" data-icon="&#xe026;"></span></button>
    	        </div>
    	        
    	        <div class="button-box speed clearfix">
    	            <button data-anim-type="ksSlow" class="swap-btn speed" title="Slow"><span aria-hidden="true" data-icon="&#xe010;"></span></button>
    	            <button data-anim-type="ksFast" class="swap-btn speed active" title="Fast"><span aria-hidden="true" data-icon="&#xe011;"></span></button>
    	            <button data-anim-type="ksUltraFast" class="swap-btn speed" title="Ultra Fast"><span aria-hidden="true" data-icon="&#xe012;"></span></button>
    	        </div>
    			
    		</div>
    	</div>
    </header>
    
    <div class="main row">
        <div id="main" class="main-inner col-md-12">
    		<a id="dl" href="https://github.com/griffla/Kool-Swap"><span aria-hidden="true" data-icon="&#xe006;"></span> Download on GitHub</a>
    

