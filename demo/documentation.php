<?php 
    $currentSite = 'documentation';
?>
<!doctype html>
<html>
    
<head>
    <meta charset="utf-8">
    <title>Documentation - jQuery Kool Swap!</title>
    <meta name="author" content="Joscha Schmidt">
    <meta name="description" content="Kool Swap documentation - Get to know Kool Swap!">
    <meta name="viewport" content="width=device-width, minimum-scale=1.0, maximum-scale=1.0" />
    
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/script_head.php'; 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/link_head.php'; 
	?>
</head>

<body>
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/page_header.php'; 
    ?>
    <div class="row">
        <div class="col-md-12">
            <h1>Options</h1>
            <?php 
            	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/social_box.php'; 
            ?>
            
            <p>These options are available to configure the plugin for your needs. The default animation type - if no easing is defined - is fade. Define easings to change the animation type.</p>
            
            <table>
                <thead>
                    <tr>
                        <th>Option</th>
                        <th>Default</th>
                        <th>Description</th>
                    </tr>
                </thead>
                
                <tbody>
                    <tr>
                        <td>swapBox</td>
                        <td></td>
                        <td>Define the element that should be swaped. It can be any CSS selector but ideally an ID.</td>
                    </tr>
                    <tr>
                        <td>loadBox</td>
                        <td></td>
                        <td>Normally the contents from a box with the same ID as the swapBox are loaded from the target page. Get the content from any other selector on the target page with loadBox.</td>
                    </tr>
                    <tr>
                        <td>swapTriggerBox</td>
                        <td>.kool-swap</td>
                        <td>Element that contains the triggers that will be used for a kool swap.</td>
                    </tr>
                    <tr id="swap-trigger">
                        <td>swapTrigger</td>
                        <td>a</td>
                        <td>Trigger elements inside swapTriggerBox.</td>
                    </tr>
                    <tr>
                        <td>direction</td>
                        <td></td>
                        <td>No direction means that pages are faded in/out. Possible values are: left-to-right, right-to-left, top-to-bottom and bottom-to-top.</td>
                    </tr>
                    <tr>
                        <td>inDuration</td>
                        <td>700</td>
                        <td>Duration for the loading page.</td>
                    </tr>
                    <tr>
                        <td>outDuration</td>
                        <td>500</td>
                        <td>Duration for the unloading page.</td>
                    </tr>
                    <tr>
                        <td>inEasing</td>
                        <td>easeInSine</td>
                        <td>Easing for the page swapped in. This only takes effect if a direction is defined.</td>
                    </tr>
                    <tr>
                        <td>outEasing</td>
                        <td>easeInSine</td>
                        <td>Easing for the page swapped out. This only takes effect if a direction is defined.</td>
                    </tr>
                    <tr id="bouncingBoxes">
                        <td>bouncingBoxes</td>
                        <td></td>
                        <td>Are there bouncing boxes? Define them here and they will fade out/in on page swap. <a href="/demo/examples/bouncing-boxes.php">See an example</a></td>
                    </tr>
                    <tr id="bouncingBoxHandling">
                        <td>bouncingBoxHandling</td>
                        <td>fade</td>
                        <td>Handling of bouncingBoxes. Values: "fade" and "slide"</td>
                    </tr>
                    <tr>
                        <td>preloadImages</td>
                        <td>true</td>
                        <td>All images in the page that is currently loaded are reloaded before the page swap will be executed.</td>
                    </tr>
                    <tr id="positionType">
                        <td>positionType</td>
                        <td>fixed</td>
                        <td>The swapBox will be fixed positioned by default. Position it absolute if this causes layout problems.</td>
                    </tr>
                    <tr id="moveSwapBoxClasses">
                        <td>moveSwapBoxClasses</td>
                        <td></td>
                        <td>Classes of the swapBox won't be moved to the swapBoxIn by default. Set this to true if you need to use the same class for miscellaneous contents.</td>
                    </tr>
                    <tr id="history" class="kool-swap">
                        <td>history</td>
                        <td>false</td>
                        <td>Set to true to enable history for <a href="/demo/setup.php#section-use">section-use</a></td>
                    </tr>
                    <tr id="outerWidth">
                        <td>outerWidth</td>
                        <td>false</td>
                        <td>By default the width of the swapBox is calculated by .width(). Set to "true" to use ".outerWidth()" instead.</td>
                    </tr>
                    <tr>
                        <td>loadErrorMessage</td>
                        <td>The requested page was not found.</td>
                        <td>Change to show another text on error.</td>
                    </tr>
                    <tr>
                        <td>loadErrorBacklinkText</td>
                        <td>Go back to the last page</td>
                        <td>Change to show another back link text</td>
                    </tr>
                </tbody>
                
            </table>
            
            <section id="events">
                <h1>Events</h1>
                <p>
                    <code>$(document).ready()</code> function wont be triggered after a Kool Swap page load. Other plugins or ready-functions should be reinitialised after the load or animation of a page executed by Kool Swap. Use
                    the callback events to do exactly that. 
                </p>
                
                <table>
                    <thead>
                        <tr>
                            <th>Event</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <tr>
                            <td>ksLoadCallback</td>
                            <td>Triggered after the page is loaded.</td>
                        </tr>
                        <tr>
                            <td>ksSwapCallback</td>
                            <td>Triggered after the page is loaded <strong>and</strong> animated.</td>
                        </tr>
                    </tbody>
                </table>
                
                <pre>
                    <code>$(document).on('ksLoadCallback', function() {<br>&nbsp;&nbsp;&nbsp;<em>functions</em><br>});</code>
                </pre>
                
				<p class="info">
					<span aria-hidden="true" data-icon="&#xe004;"></span>
					Ready functions that are called after "ksLoadCallback" should often not be executed in the swapBox then in the swapInBox. To do that associate callback functions to ".ks-swap-box-in".
				</p> 
                
                <p></p>
            </section>
            
            <section id="methods">
                <h1>Methods</h1>
                
                <table>
                    <thead>
                        <tr>
                            <th>Method</th>
                            <th>Description</th>
                        </tr>
                    </thead>
                    
                    <tbody>
                        <tr>
                            <td>destroy</td>
                            <td>Unbind Kool Swap click event from Kool Swap triggers.</td>
                        </tr>
                    </tbody>
                </table>
                
                <pre>
                    <code>$.koolSwap('destroy');</code>
                </pre>
                
                <h2>Update Settings</h2>
                <pre>
                    <code>$.koolSwap({<br>&nbsp;&nbsp;&nbsp;setting: 'new/changed value';<br>});</code>
                </pre>
            </section>
        </div>
    </div>
		        
    <?php 
    	require_once $_SERVER['DOCUMENT_ROOT'] . '/demo/include/page_footer.php'; 
    ?>
</body>
</html>