<!doctype html>
<html>
    
<head>
    <meta charset="utf-8">
    <title>Kool Swap</title>
    <meta name="author" content="Joscha Schmidt">
    <meta name="description" content="Ajax page swap with history support and optional effects with jQuery Kool Swap!">

    <link rel="stylesheet" type="text/css" href="/demo/css/bootstrap.min.css">
    <link rel="stylesheet" type="text/css" href="/css/kool-swap.css">
    <link rel="stylesheet" type="text/css" href="/demo/css/demo.css">
    <link href='http://fonts.googleapis.com/css?family=Lato:300,400,900' rel='stylesheet' type='text/css'>
    
</head>

<body>
	<div class="container">
        <div class="main">
		    <div id="main" class="main-inner col-md-12">
                <a id="dl" href="https://github.com/griffla/Kool-Swap">Watch at GitHub</a>
                
                <h1>Hi at Kool Swap</h1>
                <p><a href="demo">Watch the demo</a></p>
            </div>
        </div>
    </div>

    <script src="/demo/js/googleanalytics.js"></script>
</body>
</html>